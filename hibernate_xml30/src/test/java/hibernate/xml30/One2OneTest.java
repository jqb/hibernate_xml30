package hibernate.xml30;


import junit.framework.TestCase;
import org.hibernate.Session;

public class One2OneTest extends TestCase {

	public void testSave1(){
		Session session=null;
		try{
			session= HibernateUtil.getSession();
			session.beginTransaction();
			IdCard idCard=new IdCard();
			idCard.setCardNo("999919283");
			
			Person person=new Person();
			person.setName("ppp");
			person.setIdCard(idCard);
			//不会出现TransientObjectException异常
			//因为一对一主键关联映射中,默认了cascade属性
			session.save(person);
			
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally{
			HibernateUtil.closeSession(session);
		}
	}
	
	public void testLoad1(){
		Session session=null;
		try{
			session= HibernateUtil.getSession();
			session.beginTransaction();
			Person person= (Person)session.load(Person.class, 1);
			String name=person.getName();
			String cardNo=person.getIdCard().getCardNo();
			System.out.println("name="+name);
			System.out.println("cardNo="+cardNo);
			
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally{
			HibernateUtil.closeSession(session);
		}
	}
	
	public void testLoad2(){
		Session session=null;
		try{
			session= HibernateUtil.getSession();
			session.beginTransaction();
			IdCard idCard= (IdCard)session.load(IdCard.class, 1);
			String cardNo=idCard.getCardNo();
			String personname=idCard.getPerson().getName();
			System.out.println("personname="+personname);
			System.out.println("cardNo="+cardNo);
			
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
		}finally{
			HibernateUtil.closeSession(session);
		}
	}
}
